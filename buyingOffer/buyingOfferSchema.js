// Load required packages
var mongoose = require('mongoose');

// Define our movie schema
var BuyingofferSchema   = new mongoose.Schema({
    title: String,
    description: String,
    condition: String,
    price: Number,
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

// Export the Mongoose model
module.exports = mongoose.model('Buyingoffer', BuyingofferSchema);