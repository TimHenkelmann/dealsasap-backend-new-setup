var Buyingoffer = require('./buyingofferSchema');



exports.postBuyingoffer = function(req, res) {

    var buyingoffer = new Buyingoffer(req.body);

    //do not allow user to fake identity. The user who posted the buying offer must be the same user that is logged in
    //STILL NEEDS IMPLEMENTATION! Currently fails with error
    if (!req.user.equals(buyingoffer.user)) {
        res.sendStatus(401);
        return;
    }

    buyingoffer.save(function(err, m) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.status(201).json(m);
    });
};


// Create endpoint /api/buyingoffers for GET
exports.getBuyingoffers = function(req, res) {
    Buyingoffer.find(function(err, buyingoffers) {
        if (err) {
            res.status(500).send(err);
            return;
        }
        res.json(buyingoffers);
    });
};


// Create endpoint /api/buyingoffers/:buyingoffer_id for GET
exports.getBuyingoffer = function(req, res) {
    // Use the Beer model to find a specific beer
    Buyingoffer.findById(req.params.buyingoffer_id, function(err, buyingoffer) {
        if (err) {
            res.status(500).send(err)
            return;
        };

        res.json(buyingoffer);
    });
};

// Create endpoint /api/buyingoffers/:buyingoffer_id for PUT
exports.putBuyingoffer = function(req, res) {
    // Use the Beer model to find a specific beer
    Buyingoffer.findByIdAndUpdate(
        req.params.buyingOffer_id,
        req.body,
        {
            //pass the new object to cb function
            new: true,
            //run validations
            runValidators: true
        }, function (err, buyingoffer) {
            if (err) {
                res.status(500).send(err);
                return;
            }
            res.json(buyingoffer);
        });

};

// Create endpoint /api/buyingOffers/:buyingoffer_id for DELETE
exports.deleteBuyingoffer = function(req, res) {
    // Use the Beer model to find a specific beer and remove it
    Buyingoffer.findById(req.params.buyingoffer_id, function(err, buyingoffer) {
        if (err) {
            res.status(500).send(err)
            return;
        };

        //authorize
        if (buyingoffer.user && req.user.equals(buyingoffer.user)) {
            buyingoffer.remove();
            res.sendStatus(200);
        } else {
            res.sendStatus(401);
        }

    });
};
